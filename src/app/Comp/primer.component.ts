import {Component} from '@angular/core';

@Component({
    selector:'hello-world',
    templateUrl:'./primer.component.html',
    styleUrls:['./primer.component.css'],
    /*template:`<div class="hello-world">
        <h1> hello world </h1>
        <h2> hello world </h2>
    </div>`,
    */
    //styles:['h1 {background-color:blue,color:#ccc}']
})

export class HelloComponent{
    title:'bienvenido a angular'
}