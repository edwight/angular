import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:String = 'my primera app en angular 6';
  descripcion:string = 'ejemplo de descripcion';
  users = ['ana','miguel','edwight','maria'];
  activated:boolean=true;

  name:string="edwight";
  age:number;
  address:{
    street:String;
    city:String;
  };
  hobbies:String[];

  constructor(){
    this.age=28;
    this.address = {
      street:'3422 backer street',
      city:'london'
    };
    this.hobbies=['nadar','leer','musica'];
  }
  //metodo
  alerta(){
    alert('alerta desde angular');
  }
  deleteUser(user){
    for(let i=0;i<this.users.length;i++){
      if(user==this.users[i]){
        this.users.splice(i, 1);
      }
    }
  }
  addUser(newUser){
    this.users.push(newUser.value);
    newUser.value='';
    newUser.focus();
    return false;
  }
}
