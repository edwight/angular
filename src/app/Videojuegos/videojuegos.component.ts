import { Component } from '@angular/core';

@Component({
    selector:'videojuegos',
    templateUrl:'./videojuegos.component.html',
    styleUrls:['./videojuegos.component.css'],
})

export class VideojuegosComponent {
    public nombre:string;
    public mejor_juego_retro:string;
    public mejor_juego:string;
    public color:string;
    public mostrar_retro:boolean;
    public hasError:boolean;
    public canSave:boolean; 
    public year:number;
    public videojuego:Array<string>;
    public currentStyles:object;

    constructor(){
        this.nombre = 'videojuego 2018';
        this.color = 'yellow';
        this.mejor_juego = 'gta 5';
        this.mejor_juego_retro = 'super mario 64';
        this.mostrar_retro = true;
        this.year = 2018;
        this.hasError = false;
        this.canSave = false;
        this.videojuego = [
            'gta 5',
            'silent hill',
            'the last us fast',
            'call of duty'
        ];
        this.currentStyles = {     
            'font-style':  this.canSave  ? 'italic' : 'normal',  
            'color':       this.hasError ? 'red'   : 'black',     
            'font-size':   this.hasError ? '24px'   : '12px'   
     };
    };
}

  